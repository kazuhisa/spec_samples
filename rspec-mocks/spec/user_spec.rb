require './user'

describe User do
  it '名前と年齢を引数としてインスタンスが作成できること' do
    user = User.new('やまもと', 39)
    expect(user.name).to eq 'やまもと'
    expect(user.age).to eq 39
  end

  context '名前と年齢を引数としてインスタンスを作成する' do
    before do
      @user = User.new('やまもと', 39)
    end
    it "名前が取得できること" do
      expect(@user.name).to eq 'やまもと'
    end
    it "年齢が取得できること" do
      expect(@user.age).to eq 39
    end
  end

  context '名前と年齢を引数としてインスタンスを作成する' do
    let(:user) { User.new('やまもと', 39) }
    it "名前が取得できること" do
      expect(user.name).to eq 'やまもと'
    end
    it "年齢が取得できること" do
      expect(user.age).to eq 39
    end
  end

  context '名前と年齢を引数としてインスタンスを作成する' do
    subject { User.new('やまもと', 39) }
    it "名前が取得できること" do
      expect(subject.name).to eq 'やまもと'
    end
    it "年齢が取得できること" do
      expect(subject.age).to eq 39
    end
  end

  context 'メッセージを取得する' do
    subject { User.new('やまもと', 39) }
    before do
      allow(subject).to receive(:hour) { 10 }
    end
    it "メッセージが取得できること" do
      expect(subject.message).to eq "39歳のやまもとさん。10時ですよ。"
    end
  end

  context 'メソッドの呼び出し' do
    subject { User.new('やまもと', 39) }
    it "subject.messageの中でnameメソッドが呼ばれること" do
      expect(subject).to receive(:name).once
      subject.message
    end
    it "subject.messageの中でageメソッドが呼ばれること" do
      expect(subject).to receive(:age).once
      subject.message
    end
  end
end
