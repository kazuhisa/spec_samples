require './user'

# RRの設定
require 'rr'
RSpec.configure do |config|
  config.mock_with :rr
end

describe User do
  context 'メッセージを取得する' do
    subject { User.new('やまもと', 39) }
    before do
      stub(subject).hour { 10 }
    end
    it "メッセージが取得できること" do
      expect(subject.message).to eq "39歳のやまもとさん。10時ですよ。"
    end
  end

  context 'メソッドの呼び出し' do
    subject { User.new('やまもと', 39) }
    it "subject.messageの中でnameメソッドが呼ばれること" do
      mock(subject).name.times(1)
      subject.message
    end
    it "subject.messageの中でageメソッドが呼ばれること" do
      mock(subject).age.times(1)
      subject.message
    end
  end
end
