class User
  def initialize(name, age)
    @name = name
    @age = age
  end

  def name
    @name
  end

  def age
    @age
  end

  def hour
    Time.now.strftime("%H")
  end

  def message
    "#{age}歳の#{name}さん。#{hour}時ですよ。"
  end
end
